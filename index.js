var express = require('express');
var app = express();
var cors = require('cors');

app.use(cors());

app.get('/2A/', (req, res) => {
 var a = req.query.a|0;
 var b = req.query.b|0;
 res.send('' + (a+b));
});

app.get('/2B/', (req, res) => {
 var fullName = req.query.fullname;
 var parts = fullName && fullName.trim().split(/\s+/);
 function isValidName(parts){
   if (!parts || parts.length > 3){
     return false;
   }

   return parts.every((s) => 
      !/[\d_\/\\w]/.test(s)
   )
 }

 if (!isValidName(parts)) {	 
   res.send("Invalid fullname");
 } else {
   var surName = parts.pop().trim();	   
   var initials = parts.map((s)=>s[0].toUpperCase().trim() + '.').join(' ');
   var result = surName[0].toUpperCase() + surName.substr(1).toLowerCase()  + ' ' + initials;
   res.send(result.trim());
 }
});

var regexpArray = [
   /^(?:(?:https?:)?\/\/(?:www.)?)?[\w-.]+(?:\.com)?\/@?([\w.]+)([\?]?.*)?$/,
   /^@?(\w+)$/
];

app.get('/2C/', (req, res) => {
  var userName = req.query.username;
  for (var i = 0; i < regexpArray.length; i++){
    var r = regexpArray[i];
    if (r.test(userName)){
       res.send(userName.replace(r, '@$1'));
       return;
    }
  }
  res.send('Invalid username')
})


app.listen(3000, function(){
   console.log('My Server listerning on port 3000');
});
